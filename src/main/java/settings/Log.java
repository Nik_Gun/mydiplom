package settings;
import subsidiary.DateAndTimeHelper;

import java.io.BufferedWriter;
import java.io.FileWriter;

public class Log {
    int logLevel = 1;//0 - no log,1 - terminal,2 - file,3 - mix 1+2;
    DateAndTimeHelper dth;
    static Log instance;

    public static Log getInstance() {
        if (instance == null) {
            instance = new Log();
        }
        return instance;
    }

    String fileName;

    Log() {
        dth = DateAndTimeHelper.getInstance();
        fileName = dth.getCurrentDateAndTime().replace("-", "_").replace(":", "_");
        fileName += ".log";
    }

    public void print(String log) {
        if (logLevel == 1 || logLevel == 3) {
            System.out.println(dth.getCurrentTime() + "\t:\t" + log);
        }
        if (logLevel == 2 || logLevel == 3) {
            try {
                FileWriter fw = new FileWriter("Logs/" + fileName, true);
                BufferedWriter bw = new BufferedWriter(fw);
                bw.write(dth.getCurrentTime() + "\t:\t" + log);
                bw.newLine();
                bw.close();
            } catch (Exception e) {

            }
        }
    }
}
