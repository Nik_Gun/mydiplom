package settings;

import java.io.*;
import java.util.ArrayList;

public class Settings {
    static Settings instance;

    public static Settings getInstance() {
        if (instance == null) {
            instance = new Settings();
        }
        return instance;
    }

    public String lastInputDir;
    public String LogFile;

    Settings() {
        Log.getInstance().print("loading settings");
        try {
            File file = new File("settings.txt");

            FileReader fr = new FileReader(file);
            BufferedReader reader = new BufferedReader(fr);
            String line = reader.readLine().trim();
            int row = 0;
            while (line != null) {
                if (row == 0) {
                    LogFile = line.split("#")[0].trim();
                } else if (row == 1) {
                    lastInputDir = line.split("#")[0].trim();
                }
                row++;
                line = reader.readLine();
            }
            reader.close();
            fr.close();
            Log.getInstance().print("load settings success");
        } catch (FileNotFoundException e) {
            Log.getInstance().print("settings file not found");
        } catch (IOException e) {
            Log.getInstance().print(e.toString());
        } catch (Exception e) {
            Log.getInstance().print(e.toString());
        }
    }

    public void writeLastDir(String s) {
        try {
            File file = new File("settings.txt");

            FileReader fr = new FileReader(file);
            BufferedReader reader = new BufferedReader(fr);
            String line = reader.readLine().trim();
            ArrayList<String> lines = new ArrayList<String>();
            int row = 0;
            while (line != null) {
                if (row == 1)
                    line=(s + "# last usage dir");
                lines.add(line);
                row++;
                line = reader.readLine();
            }
            reader.close();
            fr.close();

            FileWriter fw = new FileWriter("settings.txt", false);
            BufferedWriter bw = new BufferedWriter(fw);
            for (String l : lines) {
                bw.write(l);
                bw.newLine();
            }
            bw.close();
        } catch (FileNotFoundException e) {
            Log.getInstance().print("settings file not found");
        } catch (IOException e) {
            Log.getInstance().print(e.toString());
        } catch (Exception e) {
            Log.getInstance().print(e.toString());
        }
    }
}
