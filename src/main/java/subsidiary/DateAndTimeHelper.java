package subsidiary;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class DateAndTimeHelper {
    private static DateAndTimeHelper instance;

    public static DateAndTimeHelper getInstance(){
        if (instance==null){
            instance=new DateAndTimeHelper();
        }
        return instance;
    }
    String getCurrentDate() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(cal.getTime());
    }

    public String getCurrentTime() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        return sdf.format(cal.getTime());
    }
    public String getCurrentDateAndTime() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(cal.getTime());
    }
}
