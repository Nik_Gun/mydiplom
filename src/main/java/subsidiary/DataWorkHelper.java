package subsidiary;

import def.Component;
import def.Def;
import lef.Element;
import lef.Lef;
import lef.Macro;
import lef.Rect;
import settings.Log;

import java.io.*;

public class DataWorkHelper {
    public DataWorkHelper() {
    }

    public Lef parseLefFile(String path) {
        Log.getInstance().print("parse LEF file...");
        Lef lef = new Lef();
        try {
            File file = new File(path);
            FileReader fr = new FileReader(file);
            BufferedReader reader = new BufferedReader(fr);
            String line = reader.readLine();
            while (line != null) {
                if (!line.contains("#")) {
                    if (line.contains("MACRO")) {
                        Macro macro = new Macro(line.split(" ")[1]);
                        while (true) {
                            line = reader.readLine().trim().replace(" ", " ");
                            ;
                            if (!line.contains("END " + macro.getName())) {
                                if (line.contains("CLASS")) {
                                    macro.setClassName(line.split(" ")[1]);
                                } else if (line.contains("SIZE")) {
                                    macro.setWight(Float.parseFloat(line.split(" ")[1]));
                                    macro.setHeight(Float.parseFloat(line.split(" ")[3]));
                                } else if (line.contains("SITE")) {
                                    macro.setSite(line.split("SITE")[1]);
                                } else if (line.contains("PIN") || line.contains("OBS")) {
                                    Element element = new Element();
                                    if (line.contains("PIN")) {
                                        element.setName(line.split(" ")[1]);
                                        element.setType("PIN");
                                    } else if (line.contains("OBS")) {
                                        element.setType("ONS");
                                    }
                                    while (true) {//парсим пины и obs
                                        line = reader.readLine().trim().replace("  ", " ");
                                        if (line.contains("DIRECTION")) {
                                            element.setDirection(line.split(" ")[1]);
                                        } else if (line.contains("LAYER")) {
                                            element.setLayer(line.split(" ")[1]);
                                        } else if (line.contains("RECT")) {
                                            element.addRect(new Rect(Float.parseFloat(line.split(" ")[1]), Float.parseFloat(line.split(" ")[2]), Float.parseFloat(line.split(" ")[3]), Float.parseFloat(line.split(" ")[4])));
                                        }
                                        if (line.contains("END")) {
                                            macro.addElement(element);
                                            break;
                                        }
                                    }
                                }
                            } else {
                                lef.addMacro(macro);
                                break;
                            }
                        }
                    }
                }
                line = reader.readLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Log.getInstance().print("LEF file not found");
        } catch (IOException e) {
            e.printStackTrace();
            Log.getInstance().print(e.toString());
        }
        Log.getInstance().print("parse LEF file fin");
        return lef;
    }

    public Def parseDefFile(String path, Lef lef) {
        Log.getInstance().print("parse DEF file");
        Def def = new Def(path);
        if (lef != null) {
            try {
                File file = new File(path);
                FileReader fr = new FileReader(file);
                BufferedReader reader = new BufferedReader(fr);
                String line = reader.readLine().trim();
                while (line != null) {
                    if (line.equals("END DESIGN")) {
                        break;
                    }
                    if (!line.contains("#")) {
                        if (line.contains("VERSION")) {
                            def.setVersion(line.split(" ")[1]);
                        } else if (line.contains("DESIGN")) {
                            def.setDesign(line.split(" ")[1]);
                        } else if (line.contains("UNITS")) {
                            def.setUnits_type(line.split(" ")[2]);
                            def.setUnits_value(Float.parseFloat(line.split(" ")[3]));
                        } else if (line.contains("DIEAREA")) {
                            def.setAreaStart(new float[]{Float.parseFloat(line.split(" ")[2]), Float.parseFloat(line.split(" ")[3])});
                            def.setAreaEnd(new float[]{Float.parseFloat(line.split(" ")[6]), Float.parseFloat(line.split(" ")[7])});
                        }
                        if (!line.contains("END") && line.contains("COMPONENTS")) {
                            while (true) {
                                line = reader.readLine().trim();
                                if (line.equalsIgnoreCase("END COMPONENTS")) {
                                    break;
                                } else {
                                    def.addComponent(new Component(lef.getMacros().get(line.split(" ")[2]), Float.parseFloat(line.split(" ")[6]), Float.parseFloat(line.split(" ")[7]), line.split(" ")[9]));
                                }
                            }
                        }
                    }
                    line = reader.readLine();
                    if (line != null) {
                        line = line.trim();
                    }
                }
            } catch (FileNotFoundException e) {
                Log.getInstance().print("DEF file not found");
                e.printStackTrace();
            } catch (IOException e) {
                Log.getInstance().print(e.toString());
                e.printStackTrace();
            }
        }
        Log.getInstance().print("parse DEF file fin");
        return def;
    }

}
