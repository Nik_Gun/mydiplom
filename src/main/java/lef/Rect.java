package lef;

public class Rect {
    public Rect(float lx, float ly, float ux, float uy) {
        this.lx = lx*1000;
        this.ly = ly*1000;
        this.ux = ux*1000;
        this.uy = uy*1000;
    }

    public float lx = 0, ly = 0;
    public float ux = 0, uy = 0;
}
