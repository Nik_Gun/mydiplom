package lef;
import java.util.ArrayList;

public class Macro {
    public Macro(String name) {
        this.name = name;
    }

    private String name = "";
    private String className = "";
    private float wight=0, height=0;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public float getWight() {
        return wight*1000;
    }

    public void setWight(float wight) {
        this.wight = wight;
    }

    public float getHeight() {
        return height*1000;
    }

    public void setHeight(float height) {
        this.height = height;
    }



    public ArrayList<Element> getElements() {
        return elements;
    }

    public void setElements(ArrayList<Element> elements) {
        this.elements = elements;
    }

    public String getSite() {
        return site;
    }

    private String site = "";
    ArrayList<Element> elements = new ArrayList<Element>();

    public void setSite(String site) {
        this.site=site;
    }

    public void addElement(Element element) {
        elements.add(element);
    }
}
