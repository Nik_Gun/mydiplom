package lef;

import java.util.ArrayList;

public class Element {
    ArrayList<Rect> rects = new ArrayList<Rect>();
    String direction = "";
    String type = "";

    public ArrayList<Rect> getRects() {
        return rects;
    }

    public void setRects(ArrayList<Rect> rects) {
        this.rects = rects;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLayer() {
        return layer;
    }

    public void setLayer(String layer) {
        this.layer = layer;
    }

    String name = "";
    String layer = "";

    public void addRect(Rect rect) {
        rects.add(rect);
    }
}
