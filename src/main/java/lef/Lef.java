package lef;

import java.util.ArrayList;
import java.util.HashMap;

public class Lef {
    String version = "";
    String SITE = "";
    String path = "";

    public HashMap<String,Macro> getMacros() {
        return macros;
    }

    public void setMacros(HashMap<String,Macro> macros) {
        this.macros = macros;
    }

    HashMap<String,Macro> macros = new HashMap<String, Macro>();

    public void addMacro(Macro macro) {
        macros.put(macro.getName(),macro);
    }
}
