package def;

import lef.Macro;

public class Component {
    public Component(Macro macro, float x, float y, String orientation) {
        this.macro = macro;
        this.x = x;
        this.y = y;
        this.orientation = orientation;
    }

    public Macro macro;
    public float x=0,y=0;
    public String orientation;


}
