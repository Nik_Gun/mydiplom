package def;

import java.util.ArrayList;

public class Def {
    String path = "";
    String version = "";
    float areaStart[] = {0, 0};

    public float[] getAreaStart() {
        return areaStart;
    }

    public void setAreaStart(float[] areaStart) {
        this.areaStart = areaStart;
    }

    public float[] getAreaEnd() {
        return areaEnd;
    }

    public void setAreaEnd(float[] areaEnd) {

        this.areaEnd = areaEnd;
    }

    float areaEnd[] = {0, 0};

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getDesign() {
        return design;
    }

    public void setDesign(String design) {
        this.design = design;
    }

    public String getUnits_type() {
        return units_type;
    }

    public void setUnits_type(String units_type) {
        this.units_type = units_type;
    }

    public float getUnits_value() {
        return units_value;
    }

    public void setUnits_value(float units_value) {
        this.units_value = units_value;
    }

    String design = "";
    String units_type = "";
    float units_value = 0;

    public ArrayList<Component> getComponents() {
        return components;
    }

    public void setComponents(ArrayList<Component> components) {
        this.components = components;
    }

    ArrayList<Component> components = new ArrayList<Component>();

    public void addComponent(Component component) {
        components.add(component);
    }

    public Def(String path) {
        this.path = path;
    }

}
