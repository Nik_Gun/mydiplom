package graphic;

import def.Def;
import lef.Lef;
import processing.core.PApplet;
import processing.event.MouseEvent;
import settings.Log;
import settings.Settings;
import subsidiary.DataWorkHelper;

import java.io.File;

import static graphic.Theme.*;

public class ProcessingClass extends PApplet {

    public float zoom = 700;
    UI ui;
    WorkArea workArea;

    Lef lef;
    Def def;
    Log log;
    DataWorkHelper dwh;
    float sx = 0, sy = -200;
    WaveAlg waveAlg;

    public void settings() {
        //size(200, 200);
        fullScreen(1);
    }

    public void setup() {
        ui = new UI(this, this);
        workArea = new WorkArea(this, this);
        dwh = new DataWorkHelper();
        log = Log.getInstance();

        if (Settings.getInstance().lastInputDir != null && (!Settings.getInstance().lastInputDir.equals("") || Settings.getInstance().lastInputDir.split("#")[0].replace(" ", "").length() < 2)) {
            folderSelected(new File(Settings.getInstance().lastInputDir));
        } else {
            selectFolder("Select a folder to process:", "folderSelected");
        }
        long startTime = System.nanoTime();
        waveAlg = new WaveAlg(this, this);
        long endTime = System.nanoTime();

        long duration = (endTime - startTime);
        System.out.println(duration);
    }

    float zoomSx = -1;
    float zoomSy = -1;

    public void draw() {
        background(bg_c[0], bg_c[1], bg_c[2]);
        //workArea.draw();
        waveAlg.StepsAlg();
        waveAlg.draw();

        ui.draw();


        if (mousePressed) {
            if (mouseButton == RIGHT) {
                sx += (mouseX - pmouseX);//zoom;
                sy += (mouseY - pmouseY);///zoom;

            } else if (mouseY > 30 && mouseButton == LEFT) {
                if (zoomSx != -1) {
                    stroke(zoomAreaStroke.getRed(), zoomAreaStroke.getGreen(), zoomAreaStroke.getBlue());
                    fill(zoomAreaFill.getRed(), zoomAreaFill.getGreen(), zoomAreaFill.getBlue(), zoomAreaFill.getAlpha());
                    rect(zoomSx, zoomSy, mouseX - zoomSx, mouseY - zoomSy);
                }
            }
        }
    }

    public void mouseWheel(MouseEvent event) {
        //zoom = zoom + ((float) event.getCount()) ;
        float e = ((float) event.getCount());
        if (e != 0) {
            e = 10 * e / abs(e);
            if (zoom + e > 4) {
                sx += ((mouseX - sx) / zoom) * (e);
                sy += ((mouseY - sy) / zoom) * (e);
                zoom += e;
                println(sx, sy, zoom);
            }
        }

    }

    public void mousePressed() {
        if (mouseY > 25) {
            if (mouseButton == LEFT) {
                if (zoomSx == -1) {
                    zoomSx = mouseX;
                    zoomSy = mouseY;
                }
            }
        }
    }

    public void keyReleased() {
        if (key == 'm') {

        }
    }

    public void mouseReleased() {
        if (mouseButton == LEFT) {
            if (zoomSx != -1) {
                if (zoomSx < mouseX && zoomSy > mouseY && zoomSx + 5 < mouseX) {
                    float dz = (abs(zoomSx - mouseX)) / (width);
                    zoom *= dz;
                    sx = 30 + (sx - zoomSx) / dz;
                    sy = height - 30 + (sy - zoomSy) / dz;
                    println(zoom);
                } else if (zoomSx > mouseX && zoomSy < mouseY) {
                    float dz = (width) / (abs(zoomSx - mouseX));
                    zoom *= dz;
                    sx = 30 - (sx - mouseX) / dz;
                    sy = height - 30 - (sy - mouseY) / dz;
                    println(zoom);
                }

                zoomSx = -1;
                zoomSy = -1;
            }
        }
    }

    public void folderSelected(File selection) {
        if (selection == null) {
            log.print("Window was closed or the user hit cancel.");
        } else {
            Settings.getInstance().writeLastDir(selection.getAbsolutePath() + "/");
            log.print("User selected " + selection.getAbsolutePath());
            for (File fs : selection.listFiles()) {
                if (fs.getName().contains(".lef")) {
                    lef = dwh.parseLefFile(selection.getAbsolutePath() + "/" + fs.getName());
                    break;
                }
            }
            for (File fs : selection.listFiles()) {
                if (fs.getName().contains(".def")) {
                    if (lef != null) {
                        def = dwh.parseDefFile(selection.getAbsolutePath() + "/" + fs.getName(), lef);
                        sx += 200;
                        sy += 200 + def.getAreaEnd()[1] / zoom;
                        break;
                    }
                }
            }
        }
    }
}
