package graphic;

import def.Component;
import lef.Element;
import lef.Rect;
import processing.core.PApplet;

import static graphic.Theme.*;

public class WorkArea {
    PApplet pa;
    ProcessingClass ps;


    public WorkArea(PApplet pa, ProcessingClass ps) {
        this.pa = pa;
        this.ps = ps;
    }

    boolean isRec = false;

    void draw() {
        if (ps.lef != null && ps.def != null) {
            pa.noFill();
            pa.strokeWeight(areaStrokeWeight);
            pa.stroke(areaStroke.getRed(), areaStroke.getGreen(), areaStroke.getBlue());
            pa.rect(ps.sx + ps.def.getAreaStart()[0], ps.sy + ps.def.getAreaStart()[1], ps.def.getAreaEnd()[0] / ps.zoom, -ps.def.getAreaEnd()[1] / ps.zoom);
            for (Component c : ps.def.getComponents()) {
                float cx = c.x / ps.zoom;
                float cy = -c.y / ps.zoom;
                pa.strokeWeight(componentStrokeWeight);
                pa.stroke(componentStroke.getRed(), componentStroke.getGreen(), componentStroke.getBlue());
                pa.rect(cx + ps.sx, cy + ps.sy, c.macro.getWight() / ps.zoom, -c.macro.getHeight() / ps.zoom);
                for (Element e : c.macro.getElements()) {
                    if (e.getName().equals("C")) {
                        pa.strokeWeight(componentStrokeWeight);
                        pa.stroke(255, 0, 0);
                    } else {
                        pa.strokeWeight(componentStrokeWeight);
                        pa.stroke(componentStroke.getRed(), componentStroke.getGreen(), componentStroke.getBlue());
                    }
                    for (Rect r : e.getRects()) {
                        if (e.getName().equals("C")) {
                            if (isRec) {
                                System.out.println((cx + r.lx / ps.zoom) + ":" + (cy - r.ly / ps.zoom)+ ":" + ((r.ux - r.lx) / ps.zoom)+ ":" + (-(r.uy - r.ly) / ps.zoom));
                            }
                        }
                        pa.rect(cx + r.lx / ps.zoom + ps.sx, cy - r.ly / ps.zoom + ps.sy, (r.ux - r.lx) / ps.zoom, -(r.uy - r.ly) / ps.zoom);
                    }
                }

            }
            isRec=false;
        }

    }
}
