package graphic;

import controlP5.ButtonBar;
import controlP5.CallbackEvent;
import controlP5.CallbackListener;
import controlP5.ControlP5;
import settings.Log;
import processing.core.PApplet;
import settings.Settings;
import subsidiary.DataWorkHelper;

import java.io.File;

public class UI {
    PApplet pa;
    ProcessingClass ps;
    ControlP5 cp5;
    ButtonBar upperBar;
    String[] upperBarItems = {"input", "savePNG", "recenter"};
    Log log;

    public UI(PApplet pa, ProcessingClass ps) {
        this.pa = pa;
        this.ps = ps;
        log = Log.getInstance();
        cp5 = new ControlP5(pa);
        upperBar = cp5.addButtonBar("upperBar")
                .setPosition(0, 0)
                .setSize(400, 20)
                .addItems(upperBarItems)
        ;
        upperBar.onMove(new CallbackListener() {
            public void controlEvent(CallbackEvent ev) {
                ButtonBar bar = (ButtonBar) ev.getController();
            }
        });
        upperBar.onClick(new CallbackListener() {
            public void controlEvent(CallbackEvent ev) {
                ButtonBar bar = (ButtonBar) ev.getController();
                upperBar(bar.hover());
            }
        });
    }

    public void draw() {
        pa.textSize(24);
        pa.text("xy: "+((pa.mouseX/ps.zoom-ps.sx))+" "+((ps.sy-pa.mouseY/ps.zoom)),pa.width-300,pa.height-25);
    }

    void upperBar(int n) {
        switch (n) {
            case 0:
                log.print("User try input dir");
                if (Settings.getInstance().lastInputDir != null && Settings.getInstance().lastInputDir.equals("")) {
                    pa.selectFolder("Select a folder to process:", "folderSelected", new File(Settings.getInstance().lastInputDir));
                } else {
                    pa.selectFolder("Select a folder to process:","folderSelected");
                }
                break;
            case 2:
                ps.zoom=1000;
                ps.sx=30;
                ps.sy=pa.height-30;
                break;
        }
    }
}
