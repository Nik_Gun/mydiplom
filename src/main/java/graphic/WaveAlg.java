package graphic;

import def.Component;
import lef.Element;
import lef.Rect;
import processing.core.PApplet;
import processing.core.PVector;

import java.awt.*;
import java.util.ArrayList;
import java.util.Random;

import static graphic.Theme.componentStroke;
import static graphic.Theme.componentStrokeWeight;

public class WaveAlg {
    public boolean isFin = false;
    PApplet pa;
    ProcessingClass ps;
    ArrayList<PVector> pins = new ArrayList<PVector>();
    Point a;
    Point b;

    int wave = 2;
    int faund = -1;

    int minX, minY;
    int maxX, maxY;
    int mapWidth;
    int mapHeight;
    int map[][];
    int netStep = 20;
    int maxWaveCount = 2000;
    int level = 1;
    long path = 0;
    ArrayList<Point> mst = new ArrayList<Point>();

    public WaveAlg(PApplet pa, ProcessingClass ps) {

        this.pa = pa;
        this.ps = ps;

        for (Component c : ps.def.getComponents()) {
            float cx = c.x / ps.zoom;
            float cy = -c.y / ps.zoom;
            pa.strokeWeight(componentStrokeWeight);
            pa.stroke(componentStroke.getRed(), componentStroke.getGreen(), componentStroke.getBlue());
            pa.rect(cx + ps.sx, cy + ps.sy, c.macro.getWight() / ps.zoom, -c.macro.getHeight() / ps.zoom);
            for (Element e : c.macro.getElements()) {
                if (e.getName().equals("C")) {
                    Rect r = e.getRects().get(0);
                    pa.fill(255);
                    pa.rect(cx + r.lx / ps.zoom + ps.sx, cy - r.ly / ps.zoom + ps.sy, (r.ux - r.lx) / ps.zoom, -(r.uy - r.ly) / ps.zoom);
                    pins.add(new PVector(cx + r.lx / ps.zoom, cy - r.ly / ps.zoom));
                }
            }
        }
        calcExtremums();
        for (int i = 0; i < pins.size(); i++) {
            pins.set(i, shiftpoint(pins.get(i)));
        }


        maxX += pa.abs(minX + 20);
        maxY += pa.abs(minY - 20);
        minX = 0;
        minY = 0;
        pa.println((maxX - minX) * (maxY - minY) * 16, "Byte");
        mapWidth = maxX;
        mapHeight = maxY;
        map = new int[mapHeight][mapWidth];
        for (int j = 0; j < mapHeight; j++) {
            for (int i = 0; i < mapWidth; i++) {
                map[j][i] = 1;
                for (PVector vs : pins) {
                    if (((int) vs.x) == i && ((int) vs.y) == j) {
                        map[j][i] = 0;
                        break;
                    }
                }
            }
        }
    }

    int step = 1;

    void clearMap(int ay, int ax) {
        faund = -1;
        b = null;
        int w1 = 0;
        int h1 = 0;
        int w2 = map[0].length;
        int h2 = map.length;
        if (ax - wave > h1) {
            h1 = ax - wave;
        }
        if (ax + wave < h2) {
            h2 = ax + wave;
        }
        if (ay - wave > w1) {
            w1 = ay - wave;
        }
        if (ay + wave < w2) {
            w2 = ay + wave;
        }

        wave = 2;
        for (int j = h1; j < h2; j++) {
            for (int i = w1; i < w2; i++) {
                if (map[j][i] > 1) {
                    map[j][i] = 1;
                }
            }
        }
    }

    ArrayList<Integer> lengthes = new ArrayList<Integer>();
    ArrayList<Point> possibleNode = new ArrayList<Point>();
    int length = 10000000;

    void StepsAlg() {
        long start = System.nanoTime();

        if (step == 1) {
            a = new Point((int) pins.get(111).x, (int) pins.get(111).y);
            mst.add(a);

        }
        step++;
        int tmp = mst.size();
        for (int i = 0; i < tmp; i++) {
            //  for (int i = 0; i < mst.size(); i++) {

                map[mst.get(i).y][mst.get(i).x] = -10000000;
                lengthes.add( mainLogic(mst.get(i).x, mst.get(i).y, false));
                map[mst.get(i).y][mst.get(i).x] = -10000000;
                possibleNode.add(b);

                clearMap(mst.get(i).x, mst.get(i).y);
                if (i == mst.size() - 1) {
                    int min = 0;

                    for (int j = 0; j < lengthes.size(); j++) {
                        if (lengthes.get(min) > lengthes.get(j)) {
                            min = j;
                            length = lengthes.get(min);
                        }
                    }

                    b = possibleNode.get(min);
                    mst.add(new Point(b.y, b.x));
                    //map[b.x][b.y] = -10000000;

                    //map[mst.get(i).y][mst.get(min).x] = -10000000;
                    mainLogic(mst.get(min).x, mst.get(min).y, true);
                    //map[mst.get(i).y][mst.get(min).x] = -10000000;
                    //    map[b.x][b.y] = -10000000;
                    level++;
                    // i = -1;
                    possibleNode.clear();
                    lengthes.clear();
                    clearMap(mst.get(min).x, mst.get(min).y);

                } else {

                }
            
        }
        long end = System.nanoTime();
        long elapsedTime = end - start;
        System.out.println(elapsedTime);

    }

    void nextLevel() {
        level++;
        faund = -1;
        b = null;
        for (int j = 0; j < mapHeight; j++) {
            for (int i = 0; i < mapWidth; i++) {
                if (map[j][i] > 1) {
                    map[j][i] = 1;
                }
            }
        }
    }

    void calcExtremums() {
        maxX = (int) pins.get(0).x;
        maxY = (int) pins.get(0).y;
        minX = (int) pins.get(0).x;
        minY = (int) pins.get(0).y;
        for (PVector v : pins) {
            if (minX > v.x) {
                minX = (int) v.x;
            }
            if (minY > v.y) {
                minY = (int) v.y;
            }
            if (maxX < v.x) {
                maxX = (int) v.x;
            }
            if (maxY < v.y) {
                maxY = (int) v.y;
            }
        }
        //pa.println(minX, minY, maxX, maxY);

    }

    PVector shiftpoint(PVector vs) {
        PVector v = new PVector();
        int x = (int) vs.x + 10;
        int y = (int) vs.y + 10;
        if (minX < 0) {
            x += pa.abs(minX);
        }
        if (minY < 0) {
            y += pa.abs(minY);
        }
        v.set(x, y);
        return v;
    }

    int mainLogic(int ax, int ay, boolean isBuild) {
        int tmp = ay;
        ay = ax;
        ax = tmp;
        if (map[ax + 1][ay] == 1) {
            map[ax + 1][ay] = wave;
        }
        if (map[ax - 1][ay] == 1) {
            map[ax - 1][ay] = wave;
        }
        if (map[ax][ay + 1] == 1) {
            map[ax][ay + 1] = wave;
        }
        if (map[ax][ay - 1] == 1) {
            map[ax][ay - 1] = wave;
        }
        wave++;
        while (wave < maxWaveCount && faund == -1) {

            int w1 = 0;
            int h1 = 0;
            int w2 = map[0].length;
            int h2 = map.length;
            if (ax - wave > h1) {
                h1 = ax - wave;
            }
            if (ax + wave < h2) {
                h2 = ax + wave;
            }
            if (ay - wave > w1) {
                w1 = ay - wave;
            }
            if (ay + wave < w2) {
                w2 = ay + wave;
            }

            for (int j = h1; j < h2; j++) {
                for (int i = w1; i < w2; i++) {
                    if (faund != -1)
                        break;
                    setWave(i, j);
                }
                if (faund != -1)
                    break;
            }
            wave++;
        }

        if (faund > 0 && isBuild) {
            buildRoad(b.x, b.y, wave);
        } else {
            return wave;
        }
        return 0;
    }

    void buildRoad(int bx, int by, int wave) {
   /*     int tmp = by;
        by = bx;
        bx = tmp;*/
        path++;
        if (bx + 1 < mapHeight && map[bx + 1][by] == wave - 2) {
            map[bx + 1][by] = -level;
            wave--;
            buildRoad(bx + 1, by, wave);
        }
        if (bx - 1 > 0 && map[bx - 1][by] == wave - 2) {
            map[bx - 1][by] = -level;
            wave--;
            buildRoad(bx - 1, by, wave);
        }
        if (by + 1 < mapWidth && map[bx][by + 1] == wave - 2) {
            map[bx][by + 1] = -level;
            wave--;
            buildRoad(bx, by + 1, wave);
        }
        if (by - 1 > 0 && map[bx][by - 1] == wave - 2) {
            map[bx][by - 1] = -level;
            wave--;
            buildRoad(bx, by - 1, wave);
        }
    }

    void setWave(int ax, int ay) {
        int tmp = ay;
        ay = ax;
        ax = tmp;
        //  if (ax < map[0].length - 2 && ax >= 0) {
        if (map[ax][ay] == wave - 1) {
            if (ax + 1 < map.length && map[ax + 1][ay] == 1) {
                map[ax + 1][ay] = wave;
            } else if (ax + 1 < map.length && map[ax + 1][ay] == 0) {
                faund = wave;
                b = new Point(ax + 1, ay);
            }
            if (ax - 1 >= 0 && map[ax - 1][ay] == 1) {
                map[ax - 1][ay] = wave;
            } else if (ax - 1 >= 0 && map[ax - 1][ay] == 0) {
                faund = wave;
                b = new Point(ax - 1, ay);
            }
            if (ay + 1 < map[0].length && map[ax][ay + 1] == 1) {
                map[ax][ay + 1] = wave;
            } else if (ay + 1 < map[0].length && map[ax][ay + 1] == 0) {
                faund = wave;
                b = new Point(ax, ay + 1);
            }
            if (ay - 1 >= 0 && map[ax][ay - 1] == 1) {
                map[ax][ay - 1] = wave;
            } else if (ay - 1 >= 0 && map[ax][ay - 1] == 0) {
                faund = wave;
                b = new Point(ax, ay - 1);
            }
        }
        //}
    }


    void draw() {
        pa.noFill();
        pa.stroke(255, 0, 0);
        pa.rect(ps.sx + minX, ps.sy + minY, maxX, -maxY);
        for (int j = 0; j < mapHeight; j++) {
            for (int i = 0; i < mapWidth; i++) {
                if (map[j][i] < 0) {
                    pa.fill(0, 255, 0);
                    pa.noStroke();
                    pa.ellipse((ps.sx + i), (ps.sy + j - mapHeight), 1, 1);
                }
                if (map[j][i] < -100000) {
                    pa.fill(0, 255, 255);
                    pa.noStroke();
                    pa.ellipse((ps.sx + i), (ps.sy + j - mapHeight), 7, 7);
                }/*
                if (map[j][i] > 2) {
                    pa.fill(255, 255, 0,100);
                    pa.noStroke();
                    pa.ellipse((ps.sx + i), (ps.sy + j - mapHeight), 7, 7);
                }*/
                if (map[j][i] == 0) {
                    pa.fill(255, 0, 255);
                    pa.noStroke();
                    pa.ellipse((ps.sx + i), (ps.sy + j - mapHeight), 7, 7);
                }
            }
        }
        for (int i = 0; i < mst.size(); i++) {
            pa.fill(255);
            pa.textSize(8);
            pa.text(i + "", ps.sx + mst.get(i).x, ps.sy + mst.get(i).y - mapHeight);
        }

    }
}
