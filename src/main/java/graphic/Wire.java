package graphic;

import processing.core.PVector;

import java.util.ArrayList;

public class Wire {
    int id;
    ArrayList<PVector> wire = new ArrayList<PVector>();
    PVector A = new PVector();
    PVector B = new PVector();
}
