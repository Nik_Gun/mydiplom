package graphic;

import processing.core.PApplet;

import java.awt.*;

public class Theme {
    static int[] bg_c={0,0,0};
    static Color areaStroke = new Color(255, 0, 0);
    static Color componentStroke = new Color(128, 255, 0);
    static Color zoomAreaStroke = new Color(255, 255, 255);
    static Color zoomAreaFill = new Color(255, 255, 255, 79);
    static int areaStrokeWeight =1;
    static int componentStrokeWeight =1;
}
